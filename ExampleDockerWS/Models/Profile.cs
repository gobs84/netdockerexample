﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleDockerWS.Models
{
    public class Profile
    {
        public string UserId { get; set; }
        public bool Active { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string FirstLastName { get; set; }
        public string SecondLastName { get; set; }
        public string UserName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public decimal NumberOfChildren { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string PersonalEmail { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string CellPhone { get; set; }
        public string Phone { get; set; }
    }
}
