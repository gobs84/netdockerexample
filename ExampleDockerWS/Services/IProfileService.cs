﻿using ExampleDockerWS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleDockerWS.Services
{
    public interface IProfileService
    {
        IEnumerable<Profile> GetProfiles();
    }
}
