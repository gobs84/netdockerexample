﻿using ExampleDockerWS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace ExampleDockerWS.Services
{
    public class ProfileService : IProfileService
    {
        public IEnumerable<Profile> GetProfiles()
        {
            HttpClient client = new HttpClient();
            string sessionURL = "http://192.168.1.249:9020/api/profiles";
            HttpResponseMessage response = client.GetAsync(sessionURL).Result;
            var data = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject< IEnumerable<Profile>>(data);
        }

    }
}
